<!DOCTYPE html>
<html dir="ltr" lang="en-US" >
<head>
  <meta charset="UTF-8" />
  <meta name="robots" content="index, follow" />
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <title>
    <?php if (is_home()) { ?>
    <?php bloginfo('name'); ?>
    -
    <?php bloginfo('description'); ?>
    <?php } else { ?>
    <?php wp_title($sep = ''); ?>
    -
    <?php bloginfo('name'); ?>
    <?php } ?>
  </title>
  <!-- add css stylesheets -->  
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.css" type="text/css" media="all" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/main.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/grid.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/skin.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/flexslider.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />


  <!-- mobile setting -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

  <!-- jquery -->
  <script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/jquery.js'></script>

  <!-- fancy box -->
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>

  <!-- easing -->
  <script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/jquery.easing.1.3.js'></script>

  <!-- Tabs -->
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.tools.min.js"></script>

  <!-- custom script -->
  <script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/scripts.js'></script>

  <!-- quote rotator -->
  <script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/jquery.quovolver.js'></script>

  <!-- FlexiSlider -->
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.flexslider.js"></script>

  <!-- Twitter -->
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/twitter.js"></script>

  <!-- Flickr  -->
  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jflickrfeed.min.js"></script>

  <!-- google webfont font replacement -->
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css' />

  <?php wp_head(); ?>
</head>


<body id="top">

  <div id="content">
  
    <!-- Main Header -->
    <div id="main-header">
    
      <!-- Tob Bar Container -->
      <div class="container_wrap" id="top-bar">
        <div class="container">
          <div class="five columns alignleft">
            <ul class="social_bookmarks">
              <li class="facebook"><a href="#">Join our Facebook Group</a></li>
              <li class="dribbble"><a href="#">Follow us on dribbble</a></li>
              <li class="twitter"><a href="#">Follow us on Twitter</a></li>
              <li class="forrst"><a href="#">Follow us on Forrst</a></li> 
              <li class="flickr"><a href="#">Flickr</a></li>
              <li class="vimeo"><a href="#">Follow us on Vimeo</a></li>
              <li class="diig"><a href="#">Follow us on Diig</a></li>
              <li class="tumblr"><a href="#">Follow us on Tumblr</a></li>
            </ul>
          </div>
          <div class="seven columns alignright">
            <div class="top-info">
              <span class="tel">Tel: 31.987.1234</span>
              <span class="email">Email: contato@orbit.com.br</span>
            </div>
          </div>
        </div>
      </div>
      <!-- End Tob Bar Container -->
      
      <!-- Head Container -->
      <div class="container_wrap" id="header">
        <div class="container">
          <h1 class="logo bg-logo">
            <a href="index.html">
              <img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt=""/>
            </a>
          </h1>
          <div class="main_menu">
            <div>
              <ul class="menu">
                <?php wp_list_pages('title_li='); ?>
                <!-- li><a href="index.html" class="current">Home</a></li>
                <li><a href="empresa.html">A Empresa</a></li>
                <li><a href="projetos.html">Projetos</a></li>
                <li><a href="#">Serviços</a></li>
                <li><a href="contact.html">Contato</a></li -->
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- End Head Container -->
      
      <!-- Slideshow Container -->
      <?php if (is_front_page()) { ?>
      <?php include (TEMPLATEPATH . '/slide_block.php'); ?>
      <?php } else { ?>
      <!-- Page Title Container -->
      <div class="container_wrap" id="page-title">
        <div class="container">
          <div class="full">
            <h2><?php the_title(); ?></h2>
          </div>
        </div>  
      </div>
      <!-- End Page Title Container -->
      <?php } ?>

      
    
    </div>
    <!-- End Main Header -->
