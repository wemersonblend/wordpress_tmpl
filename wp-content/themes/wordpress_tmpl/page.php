<?php get_header(); ?>
    <!-- Begin Main Container -->
    <div class="container_wrap fullsize" id="main">
      
      <!-- Begin Page and Sidebar -->
      <div class="container" id="template-blog">
      
        <div class="content eight alpha columns">
          
          <div class="full">
            <p>Nunc tincidunt aliquet labore tellus, magna <a href="#">quisque erat</a> morbi placerat donec quisque, felis sapien inceptos imperdiet wisi nec, pede donec mauris mauris cursus amet libero. Enim amet tortor dapibus cum sed, libero non pulvinar, eu nulla sit phasellus fringilla, in elit turpis congue faucibus. Ut habitasse ac, qui porta velit, aptent fermentum natoque cras id libero. Litora accumsan eget donec arcu tortor, ornare praesent at lacus sollicitudin ultrices, nec donec arcu bibendum eu, ut in et non neque, sed potenti quisque.</p>

            <p>In morbi massa nullam condimentum lacus, ligula in sociosqu feugiat porttitor varius orci, nonummy ut dui. Tempus malesuada proin wisi justo urna ipsum.</p>
          </div>

          <div class="entry-title">
            <h5><span>Nossos Serviços</span></h5>
          </div>
          
          <div class="one_half first column_container">
            <h1 class="monitor">Projetos Cad</h1>
            <p>In hac habitasse platea dictumst. Aliquam dictum felis a purus cursus in porttitor libero vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. <span class="more"><a href="#">Leia mais &rarr;</a></span></p>
          </div>  
          
          <div class="one_half column_container">
            <h1 class="blue-print1">Plotagens</h1>
            <p>Aliquam accumsan mattis diam, eu dapibus purus tristique convallis. Donec arcu nisi, iaculis a feugiat vitae, mollis non urna. Vestibulum erat diam, pretium non commodo eu. <span class="more"><a href="#">Leia mais &rarr;</a></span></p>
          </div>
              
          <div class="one_half first column_container">
            <h1 class="security">Engenharia</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae turpis metus. Pellentesque volutpat fringilla suscipit. Duis vulputate turpis eget eros egestas vulputate. Mauris eget libero nibh, nec dictum elit. <span class="more"><a href="#">Leia mais &rarr;</a></span></p>
          </div>
          
          <div class="one_half column_container">
            <h1 class="timer">Outros projetos</h1>
            <p>Aliquam accumsan mattis diam, eu dapibus purus tristique convallis. Donec arcu nisi, iaculis a feugiat vitae, mollis non urna. Vestibulum erat diam, pretium non commodo eu. <span class="more"><a href="#">Leia mais &rarr;</a></span></p>
          </div>

          <div class="full">
            <p>Vivamus mattis, metus nec aliquet ultricies, nibh orci viverra dolor, et venenatis massa lectus at neque. Proin aliquet lectus mattis purus dictum ultrices. Aliquam erat volutpat.</p>

            <blockquote>
              <p>Ut eu consectetur nisi. Praesent facilisis diam nec sapien gravida non mattis justo imperdiet. Vestibulum nisl urna, euismod sit amet congue at, bibendum non risus.</p>
            </blockquote>
                  
            <p>Aliquam non nibh diam. Ut elementum lorem sed elit vehicula sit amet posuere dolor placerat. <a href="#">Suspendisse</a> gravida, lorem nec mattis malesuada, urna odio tempor felis, et tincidunt quam quam eu lorem. Nulla id massa elit, vitae tincidunt mi. Vivamus mattis, metus nec aliquet ultricies, nibh orci viverra dolor, et venenatis massa lectus at neque. Proin aliquet lectus mattis purus dictum ultrices. Aliquam erat volutpat. Maecenas sed massa neque, et dignissim libero. Proin ac dui eu nibh feugiat viverra. Mauris ut nisl eu nibh scelerisque volutpat.</p>
          </div>

        
        </div>
        
        <div class="columns sidebar_right four">
          <div class="sidebar">
            <div class="inner_sidebar">

              <div class="widget">
                <div class="widget-title">Menu</div>
                <div class="widget-content links">
                  <ul>
                    <li><a href="home.html">Home</a></li>
                    <li><a href="empresa.html">A Empresa</a></li>
                    <li><a href="projetos.html">Projetos</a></li>
                    <li><a href="servicos.html">Serviços</a></li>
                                        <li><a href="contact.hrml">Contato</a></li>
                  </ul>
                </div>
              </div>  
            </div>


          </div>
        </div>
        
      </div>
      <!-- End Page and Sidebar -->
      
      <!-- Begin Partners -->
      <div class="container partners">
        <div class="one_fifth first column_container">
          <div class="partner-arrow"><h3 class="partners-title">Clientes</h3></div>
        </div>  
        
        <div class="one_fifth column_container">
          <a class="partners_images" href="#"><img src="images/partners/smashing.png" alt="" /></a>
        </div>
          
        <div class="one_fifth column_container">
          <a class="partners_images" href="#"><img src="images/partners/google.png" alt="" /></a>
        </div>
            
        <div class="one_fifth column_container">
          <a class="partners_images" href="#"><img src="images/partners/android.png" alt="" /></a>
        </div>
        
        <div class="one_fifth column_container">
          <a class="partners_images" href="#"><img src="images/partners/dribbble.png" alt="" /></a>
        </div>
      </div>
      <!-- End Partners -->
    
    </div>
    <!-- End Main Container -->
<?php get_footer(); ?>