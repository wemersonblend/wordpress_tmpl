<?php
/*
Template Name: MyHome
*/
?>
<?php get_header(); ?>

<div id="columns">
  <div class="html_blog first">
    <h2>What We Do<br />
      <span> Lorem ipsum dolor sit amet</span></h2>
    <div class="post-excerpt">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id tristique sem. Nunc nec ipsum sed nisi dictum mollis. Praesent malesuada mauris a odio adipiscing mollis.</p>
    </div>
    <div class="pic"><img src="<?php bloginfo('template_directory'); ?>/images/html_img_1.jpg" alt="picture" width="280" height="129" /></div>
    <a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/exents_a.gif" alt="picture" width="83" height="25" border="0" /></a></div>
  <div class="html_blog">
    <h2>Check Our Portfolio<br />
      <span> Lorem ipsum dolor sit amet</span></h2>
    <div class="post-excerpt">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id tristique sem. Nunc nec ipsum sed nisi dictum mollis. Praesent malesuada mauris a odio adipiscing mollis.</p>
    </div>
    <div class="pic"><img src="<?php bloginfo('template_directory'); ?>/images/html_img_2.jpg" alt="picture" width="280" height="129" /></div>
    <a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/exents_a.gif" alt="picture" width="83" height="25" border="0" /></a></div>
  <div class="html_blog">
    <h2>About Our Company<br />
      <span> Lorem ipsum dolor sit amet</span></h2>
    <div class="post-excerpt">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id tristique sem. Nunc nec ipsum sed nisi dictum mollis. Praesent malesuada mauris a odio adipiscing mollis.</p>
    </div>
    <div class="pic"><img src="<?php bloginfo('template_directory'); ?>/images/html_img_3.jpg" alt="picture" width="280" height="129" /></div>
    <a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/exents_a.gif" alt="picture" width="83" height="25" border="0" /></a></div>
  <div class="clr"></div>
</div>
<!--/columns -->
<?php get_footer(); ?>
