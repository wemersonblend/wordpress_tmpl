��    '      T  5   �      `     a     m     v     �     �     �     �     �     �     �               1  	   @  �   J     �       6        M     V     l     ~     �  
   �     �     �     �     �  #   �               %     +  O   7     �  �  �  
   u     �  �  �     i
  	   u
     
  )   �
     �
     �
     �
     �
               2      >     _     p  �   }     7     I  2   f     �     �     �     �     �               '     6     M  )   h     �     �     �     �  ^   �       �  2     �     �                       &                       '                                           
      $                  #   %   "                                   !            	            comment(s)  view(s)  view(s) per day <p>Sorry. No data so far.</p> All-Time Avg. Daily Views Before / after Popular Posts: Before / after each post: Block CODE SNIPPET SETTINGS Comments HTML MARKUP SETTINGS Include pages: Last Year Please visit <a href="options-general.php?page=wordpress-popular-posts/wordpress-popular-posts.php">Wordpress Popular Post Administration Page</a> to adjust its settings. Popular Posts STATS TAG SETTINGS Separate settings for the widget and the code snippet: Settings Shorten title output: Shorten title to: Show author: Show comment count: Show date: Show pageviews: Show up to: Sort posts by: Stats Tag display style: The most popular posts on your blog Time Range: Title: Today Total Views Use the Settings Manager below to tweak Wordpress Popular Posts to your liking. WIDGET SETTINGS With Wordpress Popular Posts, you can show your visitors what are the most popular entries on your blog. You can either use it as a <a href="widgets.php"><strong>Sidebar Widget</strong></a>  (<a href="http://codex.wordpress.org/Plugins/WordPress_Widgets" rel="external nofollow"><small>what's a widget?</small></a>), or place it in your templates using this handy <strong>code snippet</strong>: <em>&lt;?php if (function_exists("get_mostpopular")) get_mostpopular(); ?&gt</em>. characters posts Project-Id-Version: Wordpress Popular Posts
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-04-08 14:19-0430
PO-Revision-Date: 
Last-Translator: Héctor Cabrera <admin@rauru.com>
Language-Team: Héctor Cabrera <admin@rauru.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: English
X-Poedit-Country: VENEZUELA
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
 kommentarer visningar visningar per dag <p>Tyvärr. Inga data än så länge.</p> Alltid Medel Dagl. Visn. Före / efter Populära Inlägg Före / efter varje inlägg: Blockera KODSNUTT INSTÄLLNINGAR Kommentarer HTML uppmärkningsinställningar Inkludera sidor: Förra Året Vänligen besök <a href="options-general.php?page=wordpress-popular-posts/wordpress-popular-posts.php">Wordpress Popular Post Administrationssida</a> för att justera inställningarna. Populära Inlägg STATS-ETIKETT INSTÄLLNINGAR Olika inställningar för widgeten och kodsnutten: Inställningar Förkorta rubrikutmatningen: Förkorta rubriken till: Visa författare: Visa antal kommentarer: Visa datum: Visa sidvisningar: Visa upp till: Sortera inlägg efter: Stats-etikett visningsstil De mest populära inläggen på din blogg Tidsomfång: Rubrik: Idag Totala visningar Använd inställningshanteraren nedan för att trimma WordPress Popular Posts till ditt behov. WIDGET INSTÄLLNINGAR Med Wordpress Popularar Posts kan du visa dina besökare vilka inlägg som är mest populära i din blogg. Du kan använda tillägget antingen som en <a href="widgets.php"><strong>widget i sidopanelen</strong></a>  (<a href="http://wp-support.se/dokumentation/Anv%C3%A4ndning:Att_anv%C3%A4nda_widgets" rel="external nofollow"><small>att använda widgets</small></a>, <a href="http://codex.wordpress.org/Plugins/WordPress_Widgets" rel="external nofollow"><small>what's a widget?</small></a>), eller placera det i ditt tema genom den här händiga <strong>kodsnutten</strong>: <em>&lt;?php if (function_exists("get_mostpopular")) get_mostpopular(); ?&gt</em> tecken inlägg 