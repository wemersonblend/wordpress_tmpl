COPYRIGHT � TEMPLATESOLD.COM & TEMPLATEACCESS.COM
ALL RIGHTS RESERVED

Our WordPress theme licensing is comprised of two parts:

(1) PHP Code
The PHP code and framework is licensed under the GNU general public license.
See: http://codex.wordpress.org/GPL

(2) HTML/Design
The Photoshop design files, HTML code, CSS code, stock photo images, general theme design, 
and any related files packaged in our themes (non-PHP) are not GPL licensed. They are licensed 
and copyrighted under TemplateAccess.com/TemplateSOLD. You are **strictly prohibited** 
to redistribute or resell these files in any way.

For more information and installation help, visit:
http://www.templatesold.com/
http://www.templateaccess.com/

##########################################

Installation Instructions:

Main Theme
===================
Upload the entire theme folder to your Wordpress installation.
Upload to WordPress Themes Folder: /wp-content/themes/
It should look like this: /wp-content/themes/abc-theme/

Tip: Your theme folder contains files such as index.php, functions.php, *.php

Remember: You MUST upload these themes using FTP software, do not use any online upload tools.
Free FTP software download: http://filezilla-project.org/


Plugins
===================
If there are any plugins associated with this theme, upload these plugins 
to the plugins folder within your Wordpress installation:
Plugin Folder: /wp-content/plugins/


Activating Theme
===================
After uploading, login to your Wordpress Dashboard
1) Activate the plugins you just uploaded - Found under "Plugins"
2) Click "Change Theme"


Using Thumbnails
===================

To define a thumbnail for each post, you must use the custom field tag: 
"post-img", followed by the image url as value.

Example:
Custom Field: post-img
Value: /thumbnails/picture1.jpg


Remember
===================
Before you use this theme, we strongly recommend you install all the plugins 
located in the /plugins/ folder to ensure 100% proper theme rendering.

The following plugins are recommended for installation:

/ad-minister/ - A complete system for handling ads, including ad-rotation.
 /flickr-rss/ - Allows you to integrate Flickr photos into your site.
/yet-another-related-posts-plugin/ - Shows related WordPress posts
/wp-pagenavi/ - Adds a more advanced paging navigation to your WordPress site.
/wordpress-popular-posts/ - Shows the most popular entries on your blog

+ any other plugin found or mentioned within the theme distribution.


Extras
===================

Edit header.php - Change logo/company name
Edit footer.php - Change footer text
Edit sidebar.php - Change sidebar content

##########################################
