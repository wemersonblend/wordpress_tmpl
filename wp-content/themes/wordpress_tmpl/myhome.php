<?php
/*
Template Name: MyHome
*/
?>
<?php get_header(); ?>

  <!-- Begin Main Container -->
    <div class="container_wrap fullsize" id="main"> 
    
      <!-- Slogan Text Container -->
      <!--<div class="container_wrap">
        <div class="container">
          <div class="three_fourth first">
            <h4>This is Prometheus, a responsive business template. 40 premade pages and 20 different skins are just few of the great features that this template has.</h4>
          </div>
          <div class="one_fourth">
            <div class="buy_button"><a href="#">Buy Now</a></div>
          </div>          
        </div>
      </div>-->
      <!-- End Slogan Text Container -->
      
      <!-- Begin Projects-->
      <div class="container" id="homepage-portfolio">
      
        <div class="entry-title">
          <h5><span>Projetos</span></h5>
        </div>
      
        <div class="one_fourth first column_container">
          <div class="portfolio-image"><a href="project.html"><img src="images/portfolio/img1.png" alt="portfolio" /></a></div>
          <h1><a href="project.html">Aliquam eu eros velit</a></h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu eros velit, non blandit ipsum. Donec pretium, nibh vitae. <span class="more"><a href="#">Detalhes &rarr;</a></span></p>
        </div>

        <div class="one_fourth column_container">
          <div class="portfolio-image"><a href="project.html"><img src="images/portfolio/img2.png" alt="portfolio" /></a></div>
          <h1><a href="project.html">Consectetur adipiscing</a></h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu eros velit, non blandit ipsum. Donec pretium, nibh vitae tristique tempor. <span class="more"><a href="#">Detalhes &rarr;</a></span></p>
        </div>  
          
        <div class="one_fourth column_container">
          <div class="portfolio-image"><a href="project.html"><img src="images/portfolio/img3.png" alt="portfolio" /></a></div>
          <h1><a href="project.html">Donec pretium</a></h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu eros velit, non blandit ipsum. Donec pretium, nibh vitae tristique tempor. <span class="more"><a href="#">Detalhes &rarr;</a></span></p>
        </div>

        <div class="one_fourth column_container">
          <div class="portfolio-image"><a href="project.html"><img src="images/portfolio/img4.png" alt="portfolio" /></a></div>
          <h1><a href="project.html">Lorem ipsum dolor</a></h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu eros velit, non blandit ipsum. Donec pretium, nibh. <span class="more"><a href="#">Detalhes &rarr;</a></span></p>
        </div>        
        
      </div>
      <!-- End Projects -->

      <!-- Begin Services  -->
      <div class="container services" id="homepage-services">
      
        <div class="entry-title">
          <h5><span>NOSSOS SERVIÇOS</span></h5>
        </div>
        
        <div class="one_third first column_container">
          <h1 class="monitor">Projetos Cad</h1>
          <p>In hac habitasse platea dictumst. Aliquam dictum felis a purus cursus in porttitor libero vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. <span class="more"><a href="#">Leia mais &rarr;</a></span></p>
        </div>  
        
        <div class="one_third column_container">
          <h1 class="blue-print1">Plotagens</h1>
          <p>Aliquam accumsan mattis diam, eu dapibus purus tristique convallis. Donec arcu nisi, iaculis a feugiat vitae, mollis non urna. Vestibulum erat diam, pretium non commodo eu. <span class="more"><a href="#">Leia mais &rarr;</a></span></p>
        </div>
          
        <div class="one_third column_container">
          <h1 class="graph1">Engenharia</h1>
          <p>Aliquam ornare porta ligula sed dapibus. Vivamus viverra tincidunt tristique. Phasellus posuere consequat purus eu mattis. Nunc ut mauris augue, eu dignissim est. Duis cursus eros. <span class="more"><a href="#">Leia mais &rarr;</a></span></p>
        </div>
        
      </div>
      <!-- End Services -->
                      
      <!-- Begin Partners -->
      <div class="container partners">
        <div class="one_fifth first column_container">
          <div class="partner-arrow"><h3 class="partners-title">Clientes</h3></div>
        </div>  
        
        <div class="one_fifth column_container">
          <a class="partners_images" href="#"><img src="images/partners/smashing.png" alt="" /></a>
        </div>
          
        <div class="one_fifth column_container">
          <a class="partners_images" href="#"><img src="images/partners/google.png" alt="" /></a>
        </div>
            
        <div class="one_fifth column_container">
          <a class="partners_images" href="#"><img src="images/partners/android.png" alt="" /></a>
        </div>
        
        <div class="one_fifth column_container">
          <a class="partners_images" href="#"><img src="images/partners/dribbble.png" alt="" /></a>
        </div>
      </div>
      <!-- End Partners -->
    
    </div>
    <!-- End Main Container -->
    
<?php get_footer(); ?>
