<?php get_header(); ?>

<div id="columns">
  <div id="centercol">
    <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <div class="box post" id="post-<?php the_ID(); ?>">
      <div class="content">
        <?php $postimageurl = get_post_meta($post->ID, 'post-img', true); if ($postimageurl) { ?>
        <div class="pic"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><img src="<?php echo $postimageurl; ?>" alt="<?php the_title_attribute(); ?>" /></a></div>
        <?php } ?>
        <!--/post-img -->
        <div class="post-commets"><a href="<?php the_permalink(); ?>" title="View <?php _e('Comments'); ?>">
          <?php comments_number(0, 1, '%'); ?>
          </a></div>
        <!--/post-commets -->
        <div class="post-title">
          <h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
            <?php the_title(); ?>
            </a></h2>
        </div>
        <!--/post-title -->
        <div class="clr"></div>
        <div class="post-date">On
          <?php the_time('M.d.y'); ?>
          , In
          <?php the_category(', ') ?>
          by
          <?php the_author_posts_link(); ?>
          <div class="clr"></div>
        </div>
        <!--/post-date -->
        <div class="post-excerpt">
          <?php the_excerpt(); ?>
        </div>
        <!--/post-excerpt -->
        <div class="post-link"><a href="<?php the_permalink(); ?>" title="View <?php _e('Comments'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/exents_a.gif" alt="picture" width="83" height="25" border="0" /></a></div>
        <!--/post-link -->
        <div class="clr"></div>
      </div>
      <!--/content -->
    </div>
    <!--/box -->
    <?php endwhile; ?>
    <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
    <?php /*
        <ul>
			<li><?php next_posts_link('&laquo; Older Entries') ?></li>
			<li><?php previous_posts_link('Newer Entries &raquo;') ?></li>
		</ul>
		*/ ?>
    <?php else : ?>
    <div class="box post">
      <div class="content">
        <div class="post-title">
          <h1>No Posts Were Found</h1>
        </div>
      </div>
    </div>
    <?php endif; ?>
  </div>
  <!--/centercol -->
  <?php get_sidebar(); ?>
  <div class="clr"></div>
</div>
<!-- /columns -->
<?php get_footer(); ?>
