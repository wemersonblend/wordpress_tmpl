<?php
/*
Template Name: Page-Blog
*/

include 'includes/utils.php'
?>

<?php get_header(); ?>

    <!-- Begin Main Container -->
    <div class="container_wrap fullsize" id="main">   
      
      <!-- Begin Blog and Sidebar -->
      <div class="container" id="template-blog">
      
        <div class="content eight alpha columns">
        
          <div class="full column_container">
            
            <?php $my_cat  = removeStress(get_the_title()); ?>
            <?php $my_query = new WP_Query('category_name='.$my_cat );?>

          <!-- IF tem Posts -->
          <?php if (have_posts()) : ?>
          <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
            <div class="post">
              
              <?php $postimageurl = get_post_meta($post->ID, 'post-img', true); if ($postimageurl) { ?>
                <div class="post-image">
                  <a href="blog-post-rightsidebar.html"><img src="<?php echo $postimageurl; ?>" alt="<?php the_title_attribute(); ?>" />
                </div>
              <?php } ?>
              
              <?php
                $cat_id = get_the_category();
                $cat_description = $cat_id[0]->description; 
                $cat_cor = explode(";", $cat_description);
              ?>
              <a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?> </h1></a>
              <span class="news-author">by <a href="#">Alex Gurghis</a> on <a href="#">February 14, 2011</a> in <a href="#">Nature</a> | <a href="#">No Comments</a></span>
              
              <div class="post-content">
                <?php the_excerpt();?>
                <span class="more"><a href="<?php the_permalink(); ?>">Leia mais &rarr;</a></span>
              </div>
            </div>
          <?php endwhile; ?>
          <?php else : ?>

          <?php endif; ?>
          <!-- Fim IF tem Posts -->

            <!-- Begin Pagination-->  
            <div class="pagination">
              <span class="pagination_pages">Page 1 of 2</span>
              <span class="current">1</span><a href="#" class="inactive" >2</a>
            </div>
            <!-- End Pagination-->  
            
          </div>
        
        </div>
        
        <div class="columns sidebar_right four">
          <div class="sidebar">
            <div class="inner_sidebar">
            
              <div class="widget">
    
                <form action="#" id="search_block">
                  <div>
                  <label for="search_field_block"></label>
                  <!-- end auto clear label -->
                  
                  <input type="text" name="s" id="search_field_block" />
                  <!-- end search field -->
                  
                  <input type="submit" id="search_submit_block" value="GO" />
                  </div>  
                  <!-- end search submit -->
                </form>
                <!-- end search form -->
                
              </div>  
              
              <div class="widget">
                <div class="widget-title">Quick Links</div>
                <div class="widget-content links">
                  <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Our Services</a></li>
                    <li><a href="#">Contact Us</a></li>
                  </ul>
                </div>
              </div>
              
              <div class="widget">
                <div class="widget-title">Popular News</div>
                <div class="widget-content archive">
                  <ul class="news-wrap">
                    <li class="news-content">
                      <a class="news-link" title="" href="#">
                        <strong class="news-headline">Lorem ipsum dolor sit<span class="news-time">February 11, 2012, 1:07 am</span></strong>
                      </a>
                    </li>
                    <li class="news-content">
                      <a class="news-link" title="" href="#">
                        <strong class="news-headline">Ut dui erat, tristique nec interdum<span class="news-time">February 11, 2012, 1:07 am</span></strong>
                      </a>
                    </li>
                    <li class="news-content">
                      <a class="news-link" title="" href="#">
                        <strong class="news-headline">Etiam varius gravida blandit<span class="news-time">February 11, 2012, 1:07 am</span></strong>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              
              <div class="widget">
                <div class="widget-title">Archive</div>
                <div class="widget-content links">
                  <ul>
                    <li><a href="#">February 2012</a></li>
                    <li><a href="#">January 2012</a></li>
                    <li><a href="#">December 2011</a></li>
                    <li><a href="#">November 2011</a></li>
                    <li><a href="#">October 2011</a></li>
                    <li><a href="#">September 2011</a></li>
                    <li><a href="#">August 2011</a></li>
                    <li><a href="#">June 2011</a></li>
                  </ul>
                </div>
              </div>
              
              <div class="widget">
                <div class="widget-title">Testimonials</div>
                <div class="widget-content">
                  <blockquote>
                    <p>Ut eu consectetur nisi. Praesent facilisis diam nec sapien gravida non mattis justo imperdiet.
                    <cite>Quote Author</cite></p>
                  </blockquote>
                  
                  <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu eros velit, non blandit ipsum. Donec pretium, nibh vitae tristique tempor.
                    <cite>Quote Author</cite></p>
                  </blockquote>
                
                  <blockquote>
                    <p>Donec rutrum convallis viverra. Suspendisse vehicula, risus sit amet luctus pharetra, quam ante condimentum metus.
                    <cite>Quote Author</cite></p>
                  </blockquote>
                
                  <blockquote>
                    <p>Suspendisse ipsum urna, pellentesque eget sagittis eget, porta eget ligula. Mauris id posuere nisl. Aliquam sit amet urna lorem.
                    <cite>Quote Author</cite></p>
                  </blockquote>
                
                  <blockquote>
                    <p>Nulla facilisi. Mauris vel mattis risus. Ut nec luctus tortor. In interdum pulvinar quam non pharetra.
                    <cite>Quote Author</cite></p>
                  </blockquote>
                
                  <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu eros velit, non blandit ipsum. Donec pretium.
                    <cite>Quote Author</cite></p>
                  </blockquote>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <!-- End Blog and Sidebar -->
    
    </div>
    <!-- End Main Container -->
<?php get_footer(); ?>